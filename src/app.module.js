(function () {
    angular
        .module('app', [
            'ngMaterial',
            'ngAnimate',
            'gridster',
            'ui.bootstrap'
        ]);
})();
