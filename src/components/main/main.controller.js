(function () {
    'use strict';

    angular
      .module('app')
      .controller('MainController', MainController);

    function MainController($scope, $mdDialog) {
        'ngInject';
        var vm = this;

        vm.projects = [
            {
                name: 'Project Boost',
                technology: [
                    {
                        name: 'Unity Game Engine',
                        link: 'https://en.wikipedia.org/wiki/Unity_(game_engine)'
                    },
                    {
                        name: 'C#',
                        link: 'https://en.wikipedia.org/wiki/C_Sharp_(programming_language)'
                    }
                ],
                description: 'This is a game made in the Unity game engine, coded in C#. The player is a rocket ship that can be controlled through thrust or rotation inputs. Their objective is to get to the green landing pad without colliding into the surrounding terrain. While most of the game’s physics is automatically handled by the Unity Engine, I programmed the controls (thrust and rotation) and made the assets (player model, terrain model and particle effects) except for the sound effects which were acquired from copyright-free sources.',
                link: 'Projects/Project_Boost/index.html',
                pictures: [
                    {        
                        name: 'Project_Boost_1',
                        link: 'images/Project_Boost/Project_Boost_1.PNG'
                    },
                    {        
                        name: 'Project_Boost_2',
                        link: 'images/Project_Boost/Project_Boost_2.PNG'
                    },
                    {        
                        name: 'Project_Boost_3',
                        link: 'images/Project_Boost/Project_Boost_3.PNG'
                    }
                ]
            },
            {
                name: 'Java RMI Project',
                technology: [
                    {
                        name: 'Java RMI',
                        link: 'https://en.wikipedia.org/wiki/Java_remote_method_invocation'
                    },
                    {
                        name: 'Java',
                        link: 'https://en.wikipedia.org/wiki/Java_(programming_language)'
                    }
                ],
                description: 'This project was made for a Concurrent and Distributed systems course where students were provided unit test cases and a barebones code skeleton. Apart from the provided code, we were free to use any tools provided by the basic Java library as well as the Java RMI library. The requirements were that the system that was to be built had to be thread-safe and tolerant of client failures. \n\tThe code skeleton described a simple client-server architecture with the server holding files that can be addressed through their tags. Servers are capable of modifying the the tags a file holds, manage each of the files’ locks and performing read-write operations on those files. Clients, on the other hand can not perform those operations directly and have to communicate with the server through RMI in order to perform those operations.\n\tWhen a client is performing a read/write operation to a file, the server will attempt to have that file’s lock be released in the next three seconds. To avoid having the lock released before its work is done, the client will communicate with the server every two seconds, essentially pushing back the lock release schedule for three more seconds. This is done to avoid having a file be unavailable if the client that holds its lock crashes.',
                link: 'Projects/Java_RMI/Java_RMI.zip',
                pictures: [
                    {        
                        name: 'RMI_Slide_1',
                        link: 'images/Java_RMI/Java_RMI_slide_1.png'
                    },
                    {        
                        name: 'RMI_Slide_2',
                        link: 'images/Java_RMI/Java_RMI_slide_2.png'
                    },
                    {        
                        name: 'RMI_Slide_3',
                        link: 'images/Java_RMI/Java_RMI_slide_3.png'
                    },
                    {        
                        name: 'RMI_Slide_4',
                        link: 'images/Java_RMI/Java_RMI_slide_4.png'
                    },
                    {        
                        name: 'RMI_Slide_5',
                        link: 'images/Java_RMI/Java_RMI_slide_5.png'
                    },
                    {        
                        name: 'RMI_Slide_6',
                        link: 'images/Java_RMI/Java_RMI_slide_6.png'
                    }
                ]
            },
            {
                name: 'Java Zookeeper Project',
                technology: [
                    {
                        name: 'Apache Zookeeper',
                        link: 'https://en.wikipedia.org/wiki/Apache_ZooKeeper'
                    },
                    {
                        name: 'Java RMI',
                        link: 'https://en.wikipedia.org/wiki/Java_remote_method_invocation'
                    },
                    {
                        name: 'Java',
                        link: 'https://en.wikipedia.org/wiki/Java_(programming_language)'
                    }
                ],
                description: 'This project was made for a Concurrent and Distributed systems course where students were provided unit test cases and a barebones code skeleton. Apart from the provided code, we were free to use any tools provided by the basic Java library, the Java RMI library and the Apache Zookeeper library.\n\tThe objective of this project was to create a key/value store system that maintains the consistency of the stored data across the entire system, is fault-tolerant and aware of partitions between nodes and clients. Each nodes are replicas holding portions of the same data except for the leader node which holds all of the locks associated to the keys as well as a list of the node that have those keys’ value cached. If the leader is disconnected, a new leader is elected and the previous leader will flush its list of locks keys and key holders to preserve consistency.\n\tWhen a client makes a request to get the value of a key, or set its value, if the node it is connected to is not the leader, but have the value of the key cached from a previous read or write, will simply return or set the value it has cached. If the value is not cached, the node will contact the leader node to perform that operation, then cache the current value of the key. When a new value is set for a key, the leader will contact all other nodes that have this key cached in order to have them invalidate this key which means that their cached value will be flushed.',
                link: 'Projects/Java_Zookeeper/Java_Zookeeper.zip',
                pictures: [
                    {        
                        name: 'Zookeeper_Slide_1',
                        link: 'images/Java_Zookeeper/Java_Zookeeper_slide_1.png'
                    },
                    {        
                        name: 'Zookeeper_Slide_2',
                        link: 'images/Java_Zookeeper/Java_Zookeeper_slide_2.png'
                    },
                    {        
                        name: 'Zookeeper_Slide_3',
                        link: 'images/Java_Zookeeper/Java_Zookeeper_slide_3.png'
                    },
                    {        
                        name: 'Zookeeper_Slide_4',
                        link: 'images/Java_Zookeeper/Java_Zookeeper_slide_1.png'
                    },
                    {        
                        name: 'Zookeeper_Slide_5',
                        link: 'images/Java_Zookeeper/Java_Zookeeper_slide_5.png'
                    },
                    {        
                        name: 'Zookeeper_Slide_6',
                        link: 'images/Java_Zookeeper/Java_Zookeeper_slide_6.png'
                    }
                ]
            },
            {
                name: 'Escape from Prison',
                technology: [
                    {
                        name: 'Phaser Game Framework',
                        link: 'https://en.wikipedia.org/wiki/Phaser_(game_framework)'
                    },
                    {
                        name: 'Javascript',
                        link: 'https://en.wikipedia.org/wiki/JavaScript'
                    }
                ],
                description: 'This is a game made with the Phaser Game Framework, for which all of the game\’s behaviour was coded in Javascript. The core concept of this game was the Prisoner\'s Dilemma and having players participate in this dilemma with their chance at victory at stake. The game requires three players to play, each of which are assigned a role that must remain secret until the end of the game. These roles are the two convicts and the traitor. The convicts\’ objective is to escape the prison together, whereas the traitor must escape with at least one of the convicts still stuck in prison. To escape the prison, players must answer trivia questions for which they gain points on a right answer. After that, two players are randomly selected to play in a prisoner’s dilemma, the stake is their points, if one player betrays the other, they gain points at the expense of the other. If the two player betray each other, they gain nothing. If they both trust each other, they both gain a small amount of points. A player must have certain minimum of points before triggering the end of the game where they escape as well as other players that have reached that minimum as well.\n\t This game was designed like a finite state automaton with each of the game’s phases being its own state. All game assets (images and music) were acquired from copyright-free sources.',
                link: 'Projects/Escape_From_Prison/index.html',
                pictures: [
                    {        
                        name: 'Phaser_Slide_1',
                        link: 'images/Escape_From_Prison/Escape_From_Prison_1.png'
                    },
                    {        
                        name: 'Phaser_Slide_2',
                        link: 'images/Escape_From_Prison/Escape_From_Prison_2.png'
                    },
                    {        
                        name: 'Phaser_Slide_3',
                        link: 'images/Escape_From_Prison/Escape_From_Prison_3.png'
                    },
                    {        
                        name: 'Phaser_Slide_4',
                        link: 'images/Escape_From_Prison/Escape_From_Prison_4.png'
                    }
                ]
            },
            {
                name: 'This website (casadekomenan.com)',
                technology: [
                    {
                        name: 'AngularJS',
                        link: 'https://en.wikipedia.org/wiki/AngularJS'
                    },
                    {
                        name: 'npm',
                        link: 'https://en.wikipedia.org/wiki/Npm_(software)'
                    },
                    {
                        name: 'gulp.js',
                        link: 'https://en.wikipedia.org/wiki/Gulp.js'
                    },
                    {
                        name: 'Javascript',
                        link: 'https://en.wikipedia.org/wiki/JavaScript'
                    }
                ],
                description: 'This website was developed for two purposes, first, as an online resume and portfolio that features the work I have done as part of my coursework or during my free time;  the second purpose was to apply my newly acquired web development skills while receiving assistance from my older brother who, as stated in this site’s introduction, is a software developer as well. This website was developed as a single-page application with AngularJS using NodeJS and Gulp facilitated the testing, previewing and building phases of the website. I am using an Amazon EC2 instance running Ubuntu as well as Apache to host the code.'                
            }

        ];

        vm.displayMoreDetails = function ($event, project) {
            $event.stopPropagation();
            $mdDialog.show({
                controller: 'ProjectDetailsDialogController',
                controllerAs: 'Details',
                templateUrl: 'components/project.details.dialog/projectDetailsDialog.view.html',
                targetEvent: $event,
                clickOutsideToClose: true,
                fullscreen: true,
                locals: {
                    project: project
                }
            });
        };
        // Don't worry about this stuff
        // Smooth scrolling using jQuery easing
        $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function () {
            if (location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') && location.hostname === this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    $('html, body').animate({
                        scrollTop: (target.offset().top)
                    }, 1000, 'easeInOutExpo');
                    return false;
                }
            }
        });

        // Closes responsive menu when a scroll trigger link is clicked
        $('.js-scroll-trigger').click(function () {
            $('.navbar-collapse').collapse('hide');
        });

        // Activate scrollspy to add active class to navbar items on scroll
        $('body').scrollspy({
            target: '#sideNav'
        });
    }
})();
