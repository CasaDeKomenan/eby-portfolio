(function () {
    'use strict';

    angular
      .module('app')
      .controller('ProjectDetailsDialogController', ProjectDetailsDialogController);

    function ProjectDetailsDialogController($scope, project) {
        'ngInject';
        var vm = this;
        vm.project = project;
        vm.showImages = false;
        vm.activeImage = 0;
        vm.onImageClick = function ($index) {
            vm.activeImage = $index;
            vm.showImages = true;
        };
    }
})();
