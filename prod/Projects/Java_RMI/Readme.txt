To run this project, you will need to open at least two terminal windows, one for the server and the others for the clients.

1.Run the server: java -jar target/filemanager-server-0.0.3-SNAPSHOT.jar <portnumber>
    <portnumber> can be any port number over 1024 without the "<" and ">".

2.Run the clients: ava -jar target/filemanager-client-0.0.3-SNAPSHOT.jar <portnumber>
    Make sure to use the same port number as the server.

To see the actions you can take as a client, on a client terminal, type "help"
The server will have a test directory conveniently named "testdir" that it will manage, you may add and modify the files within it as you please before running the server.
