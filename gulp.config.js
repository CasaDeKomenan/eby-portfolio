var moduleName = 'app';
var appTitle = 'CasaDeKomenan | Eby Komenan';
var productionBuild = 'production';
var stagingBuild = 'STAGING';

var output = {
    fileNames: {
        appCss: 'styles.css',
        appScripts: 'app.js',
        vendorScripts: 'vendor.js'
    },
    directories: {
        assets: 'assets',
        images: 'images',
        fonts: 'fonts',
        temp: 'temp',
        stage: 'staging',
        prod: 'prod',
        projects: 'Projects',
        resume: 'resume'
    }
};
var paths = {};
paths.src = 'src';

paths.srcFiles = {
    appScriptsGlob: [
        paths.src + '/**/*.js',
        '!' + paths.src + '/vendor/**/*.js',
        '!' + paths.src + '/Projects/**/*.*',
        '!' + paths.src + '/resume/**/*.*'
    ],
    cssGlob: [
        'node_modules/angular-material/angular-material.css',
        'node_modules/font-awesome/css/font-awesome.css',
        'node_modules/devicons/css/devicons.css',
        'node_modules/simple-line-icons/css/simple-line-icons.css',
        'node_modules/bootstrap/dist/css/bootstrap-grid.css',
        'node_modules/bootstrap/dist/css/bootstrap.css',
        'node_modules/angular-gridster/dist/angular-gridster.css',
        'node_modules/angular-ui-bootstrap/dist/ui-bootstrap-csp.css',
        paths.src + '/styles/**/*.css'
    ],
    faviconGlob: [
        // path.src + '/favicon/*'
    ],
    imageGlob: paths.src + '/images/**/*.*',
    projectsGlob: paths.src + '/Projects/**/*.*',
    resumeGlob: paths.src + '/resume/**/*.*',
    fontsGlob: paths.src + '/fonts/**/*.*',
    index: paths.src + '/index.html',
    templatesGlob: ['src/**/*.view.html'],
    workersGlob: paths.src + '/**/*.worker.js',
    vendorScriptsGlob: [
        'node_modules/jquery/dist/jquery.js',
        'node_modules/moment/moment.js',
        'node_modules/underscore/underscore.js',
        'node_modules/angular/angular.js',
        'node_modules/angular-route/angular-route.js',
        'node_modules/angular-animate/angular-animate.js',
        'node_modules/angular-aria/angular-aria.js',
        'node_modules/angular-messages/angular-messages.js',
        'node_modules/angular-material/angular-material.js',
        'node_modules/bootstrap/dist/js/bootstrap.bundle.js',
        'node_modules/jquery-easing/dist/jquery.easing.1.3.umd.js',
        'node_modules/javascript-detect-element-resize/jquery.resize.js',
        'node_modules/angular-gridster/src/angular-gridster.js',
        'node_modules/angular-ui-bootstrap/dist/ui-bootstrap-tpls.js'
    ],
    copyToBuildDirectoryGlob: [
        // empty atm
       
    ]
};

var lintGlob = [
    paths.src
];

var watch = {
    compile: [].concat(paths.srcFiles.appScriptsGlob)
        .concat(paths.srcFiles.templatesGlob)
        .concat(paths.srcFiles.workersGlob)
        .concat(paths.srcFiles.vendorScriptsGlob)
        .concat('gulpfile.js')
        .concat('gulp.config.js')
        .concat('package.json'),
    nonCompile: [].concat(paths.srcFiles.cssGlob)
        .concat(paths.srcFiles.imageGlob)
        .concat(paths.srcFiles.index)
};

module.exports = {
    moduleName: moduleName,
    appTitle: appTitle,
    productionBuild: productionBuild,
    stagingBuild: stagingBuild,
    output: output,
    paths: paths,
    lintGlob: lintGlob,
    watch: watch
};
