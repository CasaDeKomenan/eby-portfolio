module.exports = function (config) {
    config.set({
        basePath: '',
        browserDisconnectTimeout: 6000,
        reporters: ['progress', 'html'],
        files: [

        ],
        autowatch: true,
        frameworks: ['jasmine'],
        preprocessors: {
            'app/test-data/test.echo': ['json_fixtures']
        },
        browsers: ['Firefox'],
        plugins: [
            'karma-firefox-launcher',
            'karma-jasmine',
            'karma-junit-reporter',
            'karma-htmlfile-reporter',
            'karma-json-fixtures-preprocessor'
        ],
        htmlReporter: {
            outputFile: 'tests_out/test_unit.html'
        },
        junitReporter: {
            outputFile: 'test_out/test_unit.xml',
            suite: 'unit'
        }
    });
};